import math
import numpy as np
import time
import matplotlib.pyplot as plt


T = 1  # one year
K = 95.0
S0 = 100
sigma = 0.25
N = 64
r = 0.05
a = math.log(S0 / K) + r * T - 12 * math.sqrt(T * sigma ** 2)
b = math.log(S0 / K) + r * T + 12 * math.sqrt(T * sigma ** 2)
x = math.log(S0 / K)

def COS_pricing(T,K,S0,sigma,N,r,a,b,x):
	intermediate = 0
	for k in range(0, N+1):

	    chi = (1 / (1 + (k * math.pi / (b - a)) ** 2)) * \
	          (math.cos(k * math.pi) * math.exp(b) -
	           math.cos(k * math.pi * (a / (a - b))) +
	           (k * math.pi / (b - a)) * math.sin(k * math.pi) * math.exp(b) -
	           (k * math.pi / (b - a)) * math.sin(k * math.pi * (a / (a - b))))

	    if k == 0:
	        phi = b
	    else:
	        phi = ( (b - a) / (k * np.pi) ) * ( np.sin(k * np.pi) - np.sin(-k * np.pi * a / (b - a)) )
	    u = (k * np.pi) / (b - a)
	    GBM = np.e ** (1j * u * (r - (sigma ** 2) / 2.0) * T - (1 / 2.0) * (sigma ** 2) * (u ** 2) * T)

	    F_k = ((2.0) / (b - a)) * (np.e ** (1j * np.pi * k * (x - a) / (b - a)) * GBM).real
	    G_k = ((2.0) / (b - a)) * K * (chi - phi)

	    if k == 0:
	        F_k = (1 / 2.0) * F_k


	    intermediate += F_k * G_k

	    V = (np.e ** (-r * T) * ((b - a) / 2.0)) * intermediate
	return(V)

def norm_pdf(x):
    return (1.0/((2*np.pi)**0.5))*exp(-0.5*x*x)
def norm_cdf(x):
    k = 1.0/(1.0+0.2316419*x)
    k_sum = k*(0.319381530 + k*(-0.356563782 + k*(1.781477937 + k*(-1.821255978 + 1.330274429*k))))
    if x >= 0.0:
        return (1.0 - (1.0/((2*np.pi)**0.5))*math.exp(-0.5*x*x) * k_sum)
    else:
        return 1.0 - norm_cdf(-x)
def d_j(j, S, K, r, v, T):
    return (math.log(S/K) + (r + ((-1)**(j-1))*0.5*v*v)*T)/(v*(T**0.5))
def BS_pricing(S, K, r, v, T):
    return S*norm_cdf(d_j(1, S, K, r, v, T))-K*math.exp(-r*T) * norm_cdf(d_j(2, S, K, r, v, T))

print ('BS price = ', BS_pricing(S0, K, r, sigma, T)) 
print('N=', 96, 'COS price = ',  COS_pricing(T,K,S0,sigma,96,r,a,b,x))
print('N=', 192, 'COS price = ',  COS_pricing(T,K,S0,sigma,192,r,a,b,x))

resultsCos=[]
resultsBS = []
Grid = []
errorRes = []
V_BS = BS_pricing(S0, K, r, sigma, T)
endTerm = 200

for j in range(1,endTerm):
        resultsCos.append( COS_pricing(T,K,S0,sigma, j ,r,a,b,x) )
        resultsBS.append( V_BS )

for i in range(1,endTerm):

    Grid.append(i)
    error = resultsCos[i-1] - resultsBS[i-1]
    errorRes.append(math.sqrt(np.mean(error**2)))

# Plot the convergence
plt.plot(Grid, errorRes)
plt.ylabel('RMSE')
plt.xlabel('N')
plt.yscale('log')
plt.xscale('log')
plt.savefig('convergence_COS_pricing.pdf', format='pdf')
plt.show()

# Plot the pricing outcomes
plt.plot(Grid, resultsCos)
plt.ylabel('Call Price')
plt.xlabel('N')
plt.axhline(y=V_BS, color = 'g', linestyle = '--', label='Black-Scholes Price')
plt.axvline(x=96,color = 'gray')
plt.axvline(x=128,color = 'gray')
plt.axvline(x=160,color = 'gray')
plt.axvline(x=192,color = 'gray')
plt.legend()
plt.savefig('COS_prices.pdf', format='pdf')
plt.show()





# def testComplexity(dataPoints, T,K,S0,sigma,N,r,a,b,x):
#     """Tests if the algorithm as an O(n^2) timecomplexity

#     Returns two vectors of datapoints:
#     xValues    -- number of timesteps
#     yValues    -- number of seconds
#     """
#     pt = 1
#     xValues = [10*pt for pt in range(1,dataPoints)]
#     yValues = []
#     for xVal in xValues:
#         start_time = time.time()
#         COS_pricing(T,K,S0,sigma,  pt  ,r,a,b,x)
#         total_time = time.time() - start_time
#         yValues.append(total_time)
#     return(xValues,yValues)
# print(testComplexity(100,T,K,S0,sigma,N,r,a,b,x))

