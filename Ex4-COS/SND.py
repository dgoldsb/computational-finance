import math
import cmath
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

def addFirstTerm(x, start, end):
    y = 0.5 * (2/(end-start)) * math.exp(-(0**2)/2)
    
    return y

def addTerm(x, start, end, n):
    y = (2/(end-start)) * math.exp(-0.5*((n * math.pi / (end-start))**2)) * math.cos(-n*start*math.pi / (end-start)) * math.cos(n * math.pi *((x-start)/(end-start)))
    
    return y
    
def main():
    """Runs the functions above, can be ran without parameters
    """
    
    definition = 200
    start = - 10
    end = 10
    dx = (end - start)/definition
    grid = np.arange(-5,5.1,dx)
    resultGrid = np.zeros(grid.shape)
    errorX = 0
    errorY = 0
    maxTrunc = 2**6 + 1
    errorGrid = []
    errorRes = []
    
    # Set the real normal one for RMSE calculation
    normResultGrid = norm.pdf(grid)
    
    # Do the first step
    for j in range(0,len(grid)):
        resultGrid[j] = resultGrid[j] + addFirstTerm(grid[j], start, end)
    
    """
    print(grid)
    print(normResultGrid)
    print(resultGrid)
    """
	
    for i in range(1,maxTrunc):
        # print(i) # Should start at 1, not 0
        
        for j in range(0,len(grid)):
            resultGrid[j] = resultGrid[j] + addTerm(grid[j], start, end, i)
        
        # For plots
        if((i==2**1) or (i==2**2) or (i==2**3) or (i==2**4) or (i==2**5) or (i==2**6)):
            plt.plot(grid, resultGrid, label='N = '+str(i))
        
        if(i%2==1):
            # For error plot
            errorGrid.append(i)
            error = normResultGrid - resultGrid
            errorRes.append(math.sqrt(np.mean(error**2)))
            
    
    # Plot the approximations
    plt.savefig('normalplots.pdf', format='pdf')
    plt.legend()
    plt.ylabel('Probability')
    plt.xlabel('x')
    plt.show()
    
    # Plot the convergence
    plt.plot(errorGrid, errorRes)
    plt.ylabel('RMSE')
    plt.xlabel('N')
    plt.yscale('log')
    plt.xscale('log')
    plt.savefig('convergence.pdf', format='pdf')
    plt.show()
    
    print("All done.")

if __name__ == "__main__":
    main()