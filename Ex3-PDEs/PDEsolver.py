import math
import sys
import argparse
import parser
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import scipy.linalg as linalg
from scipy.interpolate import interp1d
from scipy.stats import norm

def conv(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax):
    dx = Smax / meshsizeX
    dtau = T / meshsizeT
    spacedim = np.fromiter((val*dx for val in range(0, meshsizeX)), np.float)

    #Determine your own optimal mesh size from this
    d1 = (math.log(S0/K) + (r + (volat*volat/2))*T)/(volat*math.sqrt(T))
    d2 = d1 - volat*math.sqrt(T)
    c = S0 * norm.cdf(d1) - norm.cdf(d2) * K * math.exp(-r * T)

    meshsizes = [10+x*10 for x in range(1,20)]
    FTCSerr = []
    CRNCerr = []
    meshsizes2 = []
    for meshsize in meshsizes:
        dx2 = Smax / meshsize
        spacedim2 = np.fromiter((dx2*val for val in range(0, meshsize)), np.float)
        V1 = FTCSconst(K, r, S0, volat, T, meshsize, meshsizeT, Smax, False)
        V2 = CRNC(K, r, S0, volat, T, meshsize, meshsizeT, Smax, False)
        try:
            f1 = interp1d(spacedim2, V1[:,meshsizeT-1], kind='cubic')
            f2 = interp1d(spacedim2, V2[:,meshsizeT-1], kind='cubic')
            if (meshsize==60):
                xnew = np.arange(1, 200, 0.1)
                ynew = f1(xnew)   # use interpolation function returned by `interp1d`
                xnew2 = np.arange(1, 200, 0.1)
                ynew2 = f2(xnew)   # use interpolation function returned by `interp1d`
                plt.xlabel('Spot Price')
                plt.ylabel('Call Option Value')
                plt.plot(xnew, ynew, '-', xnew2, ynew2, '-')
                plt.show()
            FTCSerr.append(math.fabs(c - f1(100)))
            CRNCerr.append(math.fabs(c - f2(100)))
            meshsizes2.append(meshsize)
        except:
            b = meshsizes.index(meshsize)
            del meshsizes[b]
    
    print(CRNCerr)
    print(FTCSerr)
    optimalMeshsize = meshsizes2[CRNCerr.index(min(CRNCerr))]
    print("Optimal mesh size for Crank-Nicholson is %d" % optimalMeshsize)
    # Do both, plot next to each other
    #plt.plot(meshsizes2, FTCSerr, marker='.', color='r')
    plt.plot(meshsizes2, CRNCerr, marker='.', color='b')
    plt.xlim(0,50)
    plt.legend(['CRNC'], loc='upper left')
    plt.xlim(0,max(meshsizes))
    plt.xlabel('Mesh width')
    plt.ylabel('Absolute error')
    plt.savefig('Convergence-findif.pdf', format='pdf')
    plt.show()

    print("SETTING SPATIAL MESHSIZE TO 100 FOR REMAINDER")
    meshsizeX = 100
    dx = Smax / meshsizeX
    dtau = T / meshsizeT
    spacedim = np.fromiter((dx*val for val in range(0, meshsizeX)), np.float)

    """Find option price for with both S0 100 110 120"""
    # x, tau
    # mind the standard parameters!
    V1 = FTCSconst(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
    V2 = CRNC(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
    f1 = interp1d(spacedim, V1[:,meshsizeT-1], kind='cubic')
    f2 = interp1d(spacedim, V2[:,meshsizeT-1], kind='cubic')
    spots = [80, 90, 100, 110, 120]
    for spot in spots:
        d1 = (math.log(spot/K) + (r + (volat*volat/2))*T)/(volat*math.sqrt(T))
        d2 = d1 - volat*math.sqrt(T)
        c = spot * norm.cdf(d1) - norm.cdf(d2) * K * math.exp(- r * T)
        #Together in a tables
        print("Spot of %d with analytical expression %.5f" % (spot, c))
        print("Spot of %d with FTCS %.5f" % (spot, f1(spot)))
        print("Spot of %d with Crank-Nicholson %.5f" % (spot, f2(spot)))
        
    #Plot DV/DS, with a border of 1 on the sides, (V n j+1 - j-1)/the 2 DS,
    #Two 3D-plots next to each other
    V1d = FTCSconst(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
    V2d = CRNC(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
    spacedimdelta = np.fromiter((dx*val for val in range(1, meshsizeX-1)), np.float)
    timedimdelta = np.arange(0, 1, dtau)
    delta1 = np.zeros((meshsizeX-2,meshsizeT))
    delta2 = np.zeros((meshsizeX-2,meshsizeT))
    for n in range(0,meshsizeT):
        for j in range(1,meshsizeX-1):
            delta1[j-1,n] = (V1d[j+1,n]-V1d[j-1,n])/(spacedim[j+1] - spacedim[j-1])
            delta2[j-1,n] = (V2d[j+1,n]-V2d[j-1,n])/(spacedim[j+1] - spacedim[j-1])
    plotDelta(delta1, spacedimdelta, timedimdelta, 'FTCS')
    plotDelta(delta2, spacedimdelta, timedimdelta, 'CRNC')

def FTCSconst(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, plotbool):
    """In this function the output is a 2D array, with M x M the timesteps and stockprice steps, and 
    the option price as the value
    """
    # Create a matrix and add the EU call boundary conditions, note that we work with tau = T - t, so we effectively reverse time
    maxindexX = meshsizeX-1
    maxindexT = meshsizeT-1
    dx = Smax / meshsizeX
    L = Smax
    E = K
    dtau = T / meshsizeT
    # x, tau
    V = np.zeros((meshsizeX,meshsizeT))
    for i in range(0,meshsizeX):
        V[i,0] = max(0,i*dx-K) # for tau = 0
        # V[i,0] = max(0,L-E) # for tau = 0

    # Make T and D matrices
    T1 = np.zeros((meshsizeX,meshsizeX))
    T2 = np.zeros((meshsizeX,meshsizeX))
    D1 = np.zeros((meshsizeX,meshsizeX))
    D2 = np.zeros((meshsizeX,meshsizeX))
    for j in range(0, meshsizeX):
        D1[j,j] = (j+1)
        D2[j,j] = (j+1)*(j+1)
        T2[j,j] = -2
        if j > 0:
            T1[j,j-1] = -1
            T2[j,j-1] = 1
        if j < maxindexX:
            T1[j,j+1] = 1
            T2[j,j+1] = 1
    F = (1-r*dtau)*np.identity((meshsizeX)) + 0.5*dtau*volat*volat*np.dot(D2,T2) + 0.5*dtau*r*np.dot(D1,T1)

    for i in range(0,meshsizeT-1):
        # Perform explicit part of time step
        tau = dtau * i
        p1 = dtau*(0.5*volat*volat-0.5*r)*0
        p2 = dtau*(maxindexX*(0.5*volat*volat*maxindexX-0.5*r)*(maxindexX*dx - K * math.exp(-r * (T - i * dtau))))
        rhs = np.dot(F,V[:,i])
        rhs[0] += (p1)
        rhs[maxindexX] += (p2)
        V[:,i+1] = rhs
        V[0,i+1] = 0
        V[maxindexX,i+1] = maxindexX*dx - K * math.exp(-r * (T - i * dtau)) 

    if plotbool: plots(meshsizeX, meshsizeT, dx, dtau, V, 'FTCS')

    return V

def FTCS(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, plotbool):
    """In this function the output is a 2D array, with M x M the timesteps and stockprice steps, and 
    the option price as the value
    """
    # Create a matrix and add the EU call boundary conditions, note that we work with tau = T - t, so we effectively reverse time
    maxindexX = meshsizeX-1
    maxindexT = meshsizeT-1
    dx = math.log(Smax) / meshsizeX
    dtau = T / meshsizeT
    # x, tau
    V = np.zeros((meshsizeX,meshsizeT))
    for i in range(0,meshsizeT):
        V[0,i] = 0
        V[maxindexX,i] = math.exp(maxindexX*dx) - K * math.exp(-r * (T - i * dtau)) # for L
    for i in range(0,meshsizeX):
        V[i,0] = max(0,math.exp(i*dx)-K) # for tau = 0

    for n in range(1,meshsizeT):
        for j in range(1,meshsizeX-1):
            V[j,n] = V[j,n-1] + dtau * ((r-(volat*volat/2))*((V[j+1,n-1]-V[j-1,n-1])/(2*dx)) 
                + (volat*volat/2)*((V[j+1,n-1] + V[j-1,n-1] - 2*V[j,n-1])/(dx*dx)) - r*V[j,n-1])

    if plotbool: plots(meshsizeX, meshsizeT, dx, dtau, V, 'FTCS')

    return V

def CRNC(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, plotbool):
    """In this function the output is a 2D array, with M x M the timesteps and stockprice steps, and 
    the option price as the value
    NOT WITH CONSTANT COEFFICIENTS
    """
    maxindexX = meshsizeX-1
    maxindexT = meshsizeT-1
    dx = Smax / meshsizeX
    L = Smax
    E = K
    dtau = T / meshsizeT
    # x, tau
    V = np.zeros((meshsizeX,meshsizeT))
    for i in range(0,meshsizeX):
        V[i,0] = max(0,i*dx-K) # for tau = 0
        # V[i,0] = max(0,L-E) # for tau = 0

    # Make T and D matrices
    T1 = np.zeros((meshsizeX,meshsizeX))
    T2 = np.zeros((meshsizeX,meshsizeX))
    D1 = np.zeros((meshsizeX,meshsizeX))
    D2 = np.zeros((meshsizeX,meshsizeX))
    for j in range(0, meshsizeX):
        D1[j,j] = (j+1)
        D2[j,j] = (j+1)*(j+1)
        T2[j,j] = -2
        if j > 0:
            T1[j,j-1] = -1
            T2[j,j-1] = 1
        if j < maxindexX:
            T1[j,j+1] = 1
            T2[j,j+1] = 1
    F = (1-r*dtau)*np.identity((meshsizeX)) + 0.5*dtau*volat*volat*np.dot(D2,T2) + 0.5*dtau*r*np.dot(D1,T1)
    B = (1+r*dtau)*np.identity((meshsizeX)) - 0.5*dtau*volat*volat*np.dot(D2,T2) - 0.5*dtau*r*np.dot(D1,T1)
    A1 = 0.5*(np.identity(meshsizeX)+F)
    A2 = 0.5*(np.identity(meshsizeX)+B)

    for i in range(0,meshsizeT-1):
        # Perform explicit part of time step
        tau = dtau * i
        p1 = dtau*(0.5*volat*volat-0.5*r)*K*math.exp(-r*tau)
        q1 = dtau*(0.5*volat*volat-0.5*r)*K*math.exp(-r*(tau+dtau))
        rhs = np.dot(A1,V[:,i])
        rhs[0] += 0.5*(p1+q1)
        Xmat = linalg.solve(A2,rhs)

        V[:,i+1] = Xmat
        V[0,i+1] = 0
        V[maxindexX,i+1] = maxindexX*dx - K * math.exp(-r * (T - i * dtau)) 

    if plotbool: plots(meshsizeX, meshsizeT, dx, dtau, V, 'CRNC')

    return V

def plotDelta(delta, spacedim, timedim, name):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    x2, y2 = np.meshgrid(timedim, spacedim)
    surf = ax.plot_surface(x2,y2,delta, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    ax.set_xlabel('Tau')
    ax.set_ylabel('Stock Value')
    ax.set_zlabel('Delta')

    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.savefig(name+'-delta.pdf',format='pdf')

def plots(meshsizeX, meshsizeT, dx, dtau, V, name):

    if name=='FTCS':
        """Make a 2D- and 3D-plot"""

        # 2D
        S = np.fromiter((math.exp(dx*val) for val in range(0, meshsizeX)), np.float)
        Vs = np.array([])
        for i in range(0,meshsizeX):
            Vs = np.append(Vs,V[i,meshsizeT-1])

        plt.plot(S,Vs, marker='.', color='r', label='FTCS')
        plt.xlim(0,200)
        plt.ylim(0,90)
        plt.xlabel('Stock Value')
        plt.ylabel('Option Value')
        plt.savefig(name+'-2D.pdf', format='pdf')
        plt.show()

        # 3D
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        tau = np.arange(0, 1, dtau)
        x2, y2 = np.meshgrid(tau, S)
        surf = ax.plot_surface(x2,y2,V, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

        ax.set_xlabel('Tau')
        ax.set_ylabel('Stock Value')
        ax.set_zlabel('Option Value')

        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.savefig(name+'-3D.pdf',format='pdf')

    else:
        """Make a 2D- and 3D-plot"""

        # 2D
        S = np.fromiter((dx*val for val in range(0, meshsizeX)), np.float)
        Vs = np.array([])
        for i in range(0,meshsizeX):
            Vs = np.append(Vs,V[i,meshsizeT-1])
        plt.plot(S,Vs, marker='.', color='r', label='FTCS')
        plt.xlim(0,200)
        plt.ylim(0,90)
        plt.xlabel('Stock Value')
        plt.ylabel('Option Value')
        plt.savefig(name+'-2D.pdf', format='pdf')
        plt.show()

        # 3D
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        tau = np.arange(0, 1, dtau)
        x2, y2 = np.meshgrid(tau, S)
        surf = ax.plot_surface(x2,y2,V, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

        ax.set_xlabel('Tau')
        ax.set_ylabel('Stock Value')
        ax.set_zlabel('Option Value')

        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.savefig(name+'-3D.pdf',format='pdf')

    """Makes tables"""

    #f = open('tables.txt', "w")
    #df = pd.DataFrame()
    #df = df.set_index('Something')  
    #f.write(df.to_latex())        
    #print(df.to_latex())
    #f.close()
    #numpy.savetxt("mydata.csv", a, delimiter=',', fmt='%2.2f')

def main(argv):
    """Runs the functions above, can be ran without parameters
    """

    parser = argparse.ArgumentParser(description='Tell the program what to run with which parameters.')
    parser.add_argument('-s','--spot', type=int, default=100, help='The spot price.')
    parser.add_argument('-L','--smax', type=int, default=1000, help='The maximum stock price.')
    parser.add_argument('-r','--inter', type=float, default=0.04, help='The interest rate.')
    parser.add_argument('-k','--strike', type=int, default=99, help='The strike price.')
    parser.add_argument('-v','--volat', type=float, default=0.3, help='The volatility.')
    parser.add_argument('-T','--matur', type=int, default=1, help='Years until maturity.')
    parser.add_argument('--meshx', type=int, default=100, help='The meshx size used.')
    parser.add_argument('--mesht', type=int, default=1000, help='The mesht size used.')
    parser.add_argument('--FTCS', action='store_true', help='Work with the FCTS scheme.')
    parser.add_argument('--FTCSconst', action='store_true', help='Work with the FCTS scheme.')
    parser.add_argument('--CRNC', action='store_true', help='Work with the Crank-Nicholson scheme.')
    parser.add_argument('--conv', action='store_true', help='Make convergence graphs.')
    args = parser.parse_args()
    
    volat = args.volat
    K = args.strike
    r = args.inter
    S0 = args.spot
    T = args.matur
    Smax = args.smax
    meshsizeX = args.meshx
    meshsizeT = args.mesht

    if args.FTCS:
        FTCS(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, True)
        print("All done with FCTS method, output saved to file.")
    elif args.FTCSconst:
        FTCSconst(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
        V2 = FTCSconst(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
        S2 = np.fromiter(((Smax/meshsizeX)*val for val in range(0, meshsizeX)), np.float)
        Vs2 = np.array([])
        for i in range(0,meshsizeX):
            Vs2 = np.append(Vs2,V2[i,meshsizeT-1])
        plt.plot(S2,Vs2, marker='.', color='b', label='FTCS w/o CC')
        plt.xlim(0,200)
        plt.ylim(0,90)
        plt.xlabel('Stock Value')
        plt.ylabel('Option Value')
        plt.legend()
        plt.savefig('compare-constcoeff.pdf', format='pdf')
        plt.show()
    elif args.CRNC:
        CRNC(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, True)
        print("All done with Crank-Nicholson method, output saved to file.")
    elif args.conv:
        conv(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax)
        # Both in same graph
    else:
        V1 = FTCS(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
        V2 = FTCSconst(K, r, S0, volat, T, meshsizeX, meshsizeT, Smax, False)
        print(V2)
        S = np.fromiter(((math.exp((math.log(Smax)/meshsizeX)*val)) for val in range(0, meshsizeX)), np.float)
        Vs = np.array([])
        S2 = np.fromiter(((Smax/meshsizeX)*val for val in range(0, meshsizeX)), np.float)
        Vs2 = np.array([])
        for i in range(0,meshsizeX):
            Vs = np.append(Vs,V1[i,meshsizeT-1])
        for i in range(0,meshsizeX):
            Vs2 = np.append(Vs2,V2[i,meshsizeT-1])
        plt.plot(S,Vs, marker='.', color='r', label='FTCS with CC')
        plt.plot(S2,Vs2, marker='.', color='b', label='FTCS w/o CC')
        plt.xlim(0,200)
        plt.ylim(0,90)
        plt.xlabel('Stock Value')
        plt.ylabel('Option Value')
        plt.legend()
        plt.savefig('compare-constcoeff.pdf', format='pdf')
        plt.show()


    print("All done.")

if __name__ == "__main__":
    main(sys.argv)