import math
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import argparse
import random
from scipy.stats import norm
from scipy.stats import mstats
import seaborn as sns
import pandas as pd
from operator import add, sub
from collections import OrderedDict

def ST(K, r, S0, T, sigma, seed):
    """Does a single Monte Carlo simulation.

    Input: parameters, seed and time of evaluation T
    Output: stock value at time T
    """
    np.random.seed(seed = seed)
    ST = S0 * math.exp((r-0.5*sigma*sigma)*T+sigma*math.sqrt(T)*np.random.normal(0,1))

    return ST

def STanti(K, r, S0, T, sigma, seed):
    """Does a single Monte Carlo simulation.

    Input: parameters, seed and time of evaluation T
    Output: stock value at time T
    """
    np.random.seed(seed = seed)
    rand = np.random.normal(0,1)
    ST1 = S0 * math.exp((r-0.5*sigma*sigma)*T+sigma*math.sqrt(T)*rand)
    ST2 = S0 * math.exp((r-0.5*sigma*sigma)*T+sigma*math.sqrt(T)*(rand*-1))

    return [ST1, ST2]

def STdigit(K, r, S0, T, sigma, seed):
    

    timesteps = 70
    

    np.random.seed(seed = seed)
    dt = T/timesteps
    STlog = [S0]
    for i in range(1,timesteps):
        rand = np.random.normal(0,1)
        Si = STlog[i-1] + r * STlog[i-1] * dt + sigma * STlog[i-1] * rand * math.sqrt(dt)
        STlog.append(Si)

    return STlog

def STasian(K, r, S0, T, sigma, seed, timesteps):
    """Does a single Monte Carlo simulation.

    Input: parameters, seed and time of evaluation T
    Output: lists stock value from T=0 to maturity
    """
    np.random.seed(seed = seed)
    dt = T/timesteps
    ST1 = [S0]
    ST1eval = []
    ST2 = [S0]
    ST2eval = []
    for i in range(1,timesteps):
        rand = np.random.normal(0,1)

        ST1.append(ST1[i-1] + r * ST1[i-1] * dt + sigma * ST1[i-1] * rand * math.sqrt(dt))
        ST2.append(ST2[i-1] + r * ST2[i-1] * dt + sigma * ST2[i-1] * (rand*-1) * math.sqrt(dt))
        ST1eval.append(ST1[i-1] + r * ST1[i-1] * dt + sigma * ST1[i-1] * rand * math.sqrt(dt))
        ST2eval.append(ST2[i-1] + r * ST2[i-1] * dt + sigma * ST2[i-1] * (rand*-1) * math.sqrt(dt))

    return [np.mean(ST1eval), np.mean(ST2eval)]
    #return [mstats.gmean(ST1eval), mstats.gmean(ST2eval)]

def callPrice(stockPrices, T, K, r, batchsize):
    payoffSum = 0
    for MCresult in stockPrices:
        payoffSum = payoffSum + max(0,MCresult-K)
    callPrice = math.exp(-r*T) * (payoffSum/batchsize)

    return callPrice

def putPrice(stockPrices, T, K, r, batchsize):
    # Call and put
    payoffSum = 0
    for MCresult in stockPrices:
        payoffSum = payoffSum + max(0,K-MCresult)
    putPrice = math.exp(-r*T) * (payoffSum/batchsize)

    return putPrice

def batchPrice(batchsize, K, r, S0, T, sigma, seed):
    """Does a batch of MC samples of variable sizes, returns a put and call price.

    Input:  size of a batch, stock parameters
    Output: EU option put and call price
    """

    MCresults = []
    for i in range(0,batchsize):
        MCresults.append(ST(K, r, S0, T, sigma, seed+i))

    return [callPrice(MCresults, T, K, r, batchsize), putPrice(MCresults, T, K, r, batchsize)]

def batchPriceAnti(batchsize, K, r, S0, T, sigma, seed):
    """Does a batch of MC samples of variable sizes, returns a call price.
    Works with antithetic variables.

    Input:  size of a batch, stock parameters
    Output: EU option call price
    """

    MCresults = []
    for i in range(0,int(batchsize/2)):
        STs = STanti(K, r, S0, T, sigma, seed+i)
        MCresults.append(STs[0])
        MCresults.append(STs[1])

    return callPrice(MCresults, T, K, r, batchsize)

def batchPriceAsian(batchsize, K, r, S0, T, sigma, seed, steps):
    """Does a batch of MC samples of variable sizes, returns an Asian call price.
    Works with antithetic variables.

    Input:  size of a batch, stock parameters
    Output: Asian option call price
    """

    MCresults = []
    for i in range(0,int(batchsize/2)):
        STs = STasian(K, r, S0, T, sigma, seed+i, steps)
        MCresults.append(STs[0])
        MCresults.append(STs[1])

    return callPrice(MCresults, T, K, r, batchsize)

def callPriceDigital(stockPrices, T, K, r, batchsize):
    payoffSum = 0
    for MCresult in stockPrices:
        payoffSum = payoffSum + heaviside(MCresult-K)
    callPrice = math.exp(-r*T) * (payoffSum/batchsize)

    return callPrice

def putPriceDigital(stockPrices, T, K, r, batchsize):
    # Call and put
    payoffSum = 0
    for MCresult in stockPrices:
        payoffSum = payoffSum + heaviside(K-MCresult)
    putPrice = math.exp(-r*T) * (payoffSum/batchsize)

    return putPrice

def batchSens(batchsize, K, r, S0, T, sigma, eps, seed, same):
    """Does a batch of MC samples of variable sizes, returns a delta.
    Note that it is possible to calculate CIs on the deltas of all batches together.

    Input:  size of a batch, stock parameters, seed
    Output: hedge parameters
    """

    bumpMCresults = []
    startSeed = seed
    for i in range(0,batchsize):
        bumpMCresults.append(ST(K, r, (S0+eps), T, sigma, (startSeed+i)))

    if (not(same)):
        startSeed = seed + batchsize
    else:
        startSeed = seed

    revalMCresults = []
    for i in range(0,batchsize):
        revalMCresults.append(ST(K, r, S0, T, sigma, (startSeed+i)))

    putPriceBump = putPrice(bumpMCresults, T, K, r, batchsize)
    callPriceBump = callPrice(bumpMCresults, T, K, r, batchsize)
    putPriceReval = putPrice(revalMCresults, T, K, r, batchsize)
    callPriceReval = callPrice(revalMCresults, T, K, r, batchsize)

    deltaPut = (putPriceBump - putPriceReval) / eps
    deltaCall = (callPriceBump - callPriceReval) / eps

    return (deltaPut, deltaCall)

def batchSensDigital(batchsize, K, r, S0, T, sigma, eps, seed, same):
    """Does a batch of MC samples of variable sizes, returns a delta.
    Note that it is possible to calculate CIs on the deltas of all batches together.

    Input:  size of a batch, stock parameters, seed
    Output: hedge parameters
    """

    bumpMCresults = []
    startSeed = seed
    for i in range(0,batchsize):
        bumpMCresults.append(ST(K, r, (S0+eps), T, sigma, (startSeed+i)))

    # if (not(same)):
    #     startSeed = seed + batchsize
    # else:
    #     startSeed = seed


    revalMCresults = []
    startSeed = seed
    for i in range(0,batchsize):
        revalMCresults.append(ST(K, r, S0, T, sigma, (startSeed+i)))

    STlogBump = []
    STlogReval = []
    for b in range(0,batchsize):
        STlogBump.append(STdigit(K, r, (S0), T, sigma, 1000))
        STlogReval.append(STdigit(K, r, (S0+eps), T, sigma, 1000))

    # write step-by-step plan on paper -> implement
    # implement Delta theoretical and plot like at Artagan (Oxford paper?), how it converges with growth of N
    # sigmoid and its plot, Oxford paper

    #putPointsBump = []
    putPointsBump = np.zeros((batchsize,70))
    callPointsBump = np.zeros((batchsize,70))
    putPointsReval = np.zeros((batchsize,70))
    callPointsReval = np.zeros((batchsize,70))
    callPointsDelta = np.zeros((batchsize,70))
    putPointsDelta = np.zeros((batchsize,70))
    # 2D arrays: each row - log of option prices, height = batchsize

    print ("STlogBump --- ROWS:",len(STlogBump), "COLUMNS:", len(STlogBump[0]))

    for b in range(0,batchsize):
        for i in range(0,70): #timesteps = 70
            putPointsBump[b][i] = math.exp(-r*T) * heaviside(K-STlogBump[b][i]) #calculate put price for each stock value
            #seed = i?
            callPointsBump[b,i] = math.exp(-r*T) * heaviside(STlogBump[b][i]-K)
            putPointsReval[b,i] = math.exp(-r*T) * heaviside(K-STlogReval[b][i])
            callPointsReval[b,i] =math.exp(-r*T) * heaviside(STlogReval[b][i]-K)
            putPointsDelta[b,i] = (putPointsBump[b,i] - putPointsReval[b,i]) / eps
            
            callPointsDelta[b,i] = (callPointsBump[b,i] - callPointsReval[b,i]) / eps
            #print ("putPriceBump",putPriceBump,"putPriceReval",putPriceReval)

    plt.plot(putPointsBump[1][:])
    plt.plot(callPointsReval[1][:])
    plt.show()

    #print("bumpMCresults[-1]", bumpMCresults[-1], "revalMCresults[-1]", revalMCresults[-1])

    putPriceBump = putPriceDigital(bumpMCresults, T, K, r, batchsize)
    callPriceBump = callPriceDigital(bumpMCresults, T, K, r, batchsize)
    putPriceReval = putPriceDigital(revalMCresults, T, K, r, batchsize)
    callPriceReval = callPriceDigital(revalMCresults, T, K, r, batchsize)

    deltaPut = (putPriceBump - putPriceReval) / eps
    deltaCall = (callPriceBump - callPriceReval) / eps


    return (deltaPut, deltaCall)

def heaviside (val):
    #Heaviside step function, returns unity when val is greater than or equal to zero and returns zero otherwise
    if val >= 0:
        return (1.0)
    else:
        return (0.0)

def batchPriceDigital(batchsize, K, r, S0, T, sigma, seed):
    """Does a batch of MC samples of variable sizes, returns digital put and call prices.

    Input:  size of a batch, stock parameters
    Output: Digital EU option put and call prices
    """

    MCresults = []
    for i in range(0,batchsize):
        MCresults.append(ST(K, r, S0, T, sigma, seed+i))

    # Call and put
    payoffSumCall = 0
    payoffSumPut = 0
    for MCresult in MCresults:
        payoffSumCall = payoffSumCall + heaviside(MCresult-K)
        payoffSumPut = payoffSumPut + heaviside(K-MCresult)

    callPrice = math.exp(-r*T) * (payoffSumCall/batchsize)
    putPrice = math.exp(-r*T) * (payoffSumPut/batchsize)
    #print ("batchPriceDigital, callPrice",callPrice,"putPrice",putPrice)

    return [callPrice, putPrice]

def batchSensDigitalLikelihoodRatio(batchsize, K, r, S0, T, sigma, eps, seed, same):
    """Does a batch of MC samples of variable sizes, returns a delta estimated using Likelihood Ratio Method.
    Note that it is possible to calculate CIs on the deltas of all batches together.

    Input:  size of a batch, stock parameters, seed
    Output: hedge parameters
    """

    MCresults = []
    deltaCallArr = []
    deltaPutArr = []
    startSeed = seed
    for i in range(0,batchsize):
        seed_Z_ST = startSeed+i
        #ST1 = ST(K, r, S0, T, sigma, seed_Z_ST)
        
        np.random.seed(seed = seed_Z_ST)
        
        Z1 = np.random.normal(0,1)
        
        ST1 = S0 * math.exp((r-0.5*sigma*sigma)*T+sigma*math.sqrt(T)*Z1)
        
        hC = heaviside(ST1-K)
        hP = heaviside(K-ST1)

        deltaCallArr.append( math.exp(-r*T) * float(hC) * Z1 / (sigma*S0*math.sqrt(T)) )
        deltaPutArr.append( math.exp(-r*T) * float(hP) * Z1 / (sigma*S0*math.sqrt(T)) )
    #average among all batches (?)
    deltaCall = sum(deltaCallArr)/batchsize
    deltaPut = sum(deltaPutArr)/batchsize
    #print ("batchSensDigitalLikelihoodRatio, deltaPut",deltaPut,"deltaCall",deltaCall, "hC",hC,"hP",hP, "Z1", Z1, "Z2", Z2)

    return (deltaPut, deltaCall)

# def plotDeltaLikelihoodRatio(batches, N, K, r, S0, T, sigma, eps, seed, same):
#     for i in range(0,batchsize):
#         #monteCarloSimsDigital(batches, N, K, r, S0, T, sigma, eps, seed, same):
#         ii=1
#     return()

def monteCarloSims(price, sens, batches, N, K, r, S0, T, sigma, eps, seed, same, anti, asian, asiansteps):
    if(price):
        callOptionPrices = []
        putOptionPrices = []
        for i in range(0,batches):
            call, put = batchPrice(N, K, r, S0, T, sigma, (seed+(i*N)))
            callOptionPrices.append(call)
            putOptionPrices.append(put)

        """Find the mean call and put and the CIs"""
        meanPut = np.mean(putOptionPrices)
        SDPut = np.std(putOptionPrices)
        CIPut = 1.98 * (SDPut/math.sqrt(batches))
        meanCall = np.mean(callOptionPrices)
        SDCall = np.std(callOptionPrices)
        CICall = 1.98 * (SDCall/math.sqrt(batches))

        return (meanPut, CIPut, meanCall, CICall)

    if(sens):
        batchResultsPut = []
        batchResultsCall = []
        for i in range(0,batches):
            deltaPut, deltaCall = batchSens(N, K, r, S0, T, sigma, eps, (seed+(2*i*N)), same)
            batchResultsPut.append(deltaPut)
            batchResultsCall.append(deltaCall)

        """Find the mean delta and the CI"""
        meanPut = np.mean(batchResultsPut)
        SDPut = np.std(batchResultsPut)
        CIPut = 1.98 * (SDPut/math.sqrt(batches))
        meanCall = np.mean(batchResultsCall)
        SDCall = np.std(batchResultsCall)
        CICall = 1.98 * (SDCall/math.sqrt(batches))

        return (meanPut, CIPut, meanCall, CICall)

    if(anti):
        callOptionPrices = []
        for i in range(0,batches):
            call = batchPriceAnti(N, K, r, S0, T, sigma, (seed+(i*N)))
            callOptionPrices.append(call)

        """Find the mean call and put and the CIs"""
        meanCall = np.mean(callOptionPrices)
        SDCall = np.std(callOptionPrices)
        CICall = 1.98 * (SDCall/math.sqrt(batches))

        return [meanCall, CICall]

    if(asian):
        callOptionPrices = []
        for i in range(0,batches):
            call = batchPriceAsian(N, K, r, S0, T, sigma, (seed+(i*N)), asiansteps)
            callOptionPrices.append(call)

        """Find the mean call and put and the CIs"""
        meanCall = np.mean(callOptionPrices)
        SDCall = np.std(callOptionPrices)
        CICall = 1.98 * (SDCall/math.sqrt(batches))

        return [meanCall, CICall]


def monteCarloSimsDigital(batches, N, K, r, S0, T, sigma, eps, seed, same):
    callOptionPrices = []
    putOptionPrices = []
    for i in range(0,batches):
        call, put = batchPriceDigital(N, K, r, S0, T, sigma, (seed+(i*N)))
        callOptionPrices.append(call)
        putOptionPrices.append(put)

    """Find the mean call and put and the CIs"""
    meanPut = np.mean(putOptionPrices)
    SDPut = np.std(putOptionPrices)
    CIPut = 1.98 * (SDPut/math.sqrt(batches))
    meanCall = np.mean(callOptionPrices)
    SDCall = np.std(callOptionPrices)
    CICall = 1.98 * (SDCall/math.sqrt(batches))

    batchResultsPutLR = []
    batchResultsCallLR = []
    for i in range(0,batches):
        deltaPutLR, deltaCallLR = batchSensDigitalLikelihoodRatio(N, K, r, S0, T, sigma, eps, (seed+(2*i*N)), same)
        batchResultsPutLR.append(deltaPutLR)
        batchResultsCallLR.append(deltaCallLR)
    
    """Find the mean delta and the CI"""
    deltaMeanPutLR = np.mean(batchResultsPutLR)
    deltaSDPutLR = np.std(batchResultsPutLR)
    deltaCIPutLR = 1.98 * (deltaSDPutLR/math.sqrt(batches))
    deltaMeanCallLR = np.mean(batchResultsCallLR)
    deltaSDCallLR = np.std(batchResultsCallLR)
    deltaCICallLR = 1.98 * (deltaSDCallLR/math.sqrt(batches))


    d2_new = (np.log(S0/float(K)) + (r-0.5*sigma*sigma)*T) / sigma*math.sqrt(T) # S0 li tut v ln??

    Nd2_new = ( 1/ math.sqrt(2*math.pi) ) * math.exp(-0.5*d2_new*d2_new)
    
    deltaPutTh  = -1* ( math.exp(-r*T) * Nd2_new) / (sigma*S0*math.sqrt(T))

    #d2 = ( np.log(S0/K) + (r-0.5*sigma*sigma)*T) / sigma*math.sqrt(T)
    #deltaCallTh = ( math.exp(-r*T) / (S0*sigma*math.sqrt(T)) ) * (1/2*math.pi ) * (math.exp(-0.5*d2*d2)) ##########(1/2*math.pi ) -> sqrt!!!
    
    deltaCallTh  = ( math.exp(-r*T) * Nd2_new) / (sigma*S0*math.sqrt(T))

    priceCallTh = math.exp(-r*T) * norm.cdf(d2_new)
    pricePutTh = math.exp(-r*T) * (1-norm.cdf(d2_new))

    deltaPutArray = []
    for STdt in range(1,200):
        d2_new = (np.log(STdt/float(K)) + (r-0.5*sigma*sigma)*T) / sigma*math.sqrt(T) 
        Nd2_new = ( 1/ math.sqrt(2*math.pi) ) * math.exp(-0.5*d2_new*d2_new)
        deltaPutArray.append(-1* ( math.exp(-r*T) * Nd2_new) / (sigma*STdt*math.sqrt(T)))

    # deltaPutArray = []
    # for STdt in range(1,200):
    #     d2_new = (np.log(STdt/float(K)) + (r-0.5*sigma*sigma)*T) / sigma*math.sqrt(T) 
    #     Nd2_new = ( 1/ math.sqrt(2*math.pi) ) * math.exp(-0.5*d2_new*d2_new)
    #     deltaPutArray.append(-1* ( math.exp(-r*T) * Nd2_new) / (sigma*STdt*math.sqrt(T)))




    #plt.plot(deltaPutArray) #schitat' po batch-am -> sravnit' s etoi teoriei
    #plt.show()
    
    #print ("meanPut", meanPut, "\tdeltaMeanPutLR", deltaMeanPutLR,"\tmeanCall", meanCall, "\tdeltaMeanCallLR", deltaMeanCallLR)

    return (pricePutTh, meanPut, CIPut, priceCallTh, meanCall, CICall, deltaMeanPutLR, deltaCIPutLR, deltaMeanCallLR, deltaCICallLR, deltaCallTh, deltaPutTh)

def plots(K, r, S0, T, sigma, eps, seed, asiansteps):
    """Makes a number of plots for the report."""

    print("All plots and tables use 100 batches, and 1000 samples per batch if not specified.")

    f = open('tables', "w")

    print("Sigma tables")
    dfsigma = pd.DataFrame()
    sigmas = [0.05, 0.1, 0.15, 0.20, 0.25, 0.30, 0.35]
    samples = 100000
    batches = 100
    N = 1000
    for varsigma in sigmas:
        print("Current sigma: %.2f" % (varsigma))

        meanPut, CIPut, meanCall, CICall = monteCarloSims(True, False, batches, 
            N, K, r, S0, T, varsigma, eps, seed, False, False, False, asiansteps)
        ATmeanCall, ATCICall = monteCarloSims(False, False, batches, 
            N, K, r, S0, T, varsigma, eps, seed, False, True, False, asiansteps)
        ASmeanCall, ASCICall = monteCarloSims(False, False, batches, 
            N, K, r, S0, T, varsigma, eps, seed, False, False, True, asiansteps)

        AntiEUcall = ATCICall/ATmeanCall
        EUcall = CICall/meanCall
        EUput = CIPut/meanPut
        AntiAsiancall = ASCICall/ASmeanCall

        dfsigma = dfsigma.append({'EU Call (AT) Option Price':AntiEUcall, 'EU Call Option Price':EUcall, 
                'EU Put Option Price':EUput, 'Asian Call (AT) Option Price':AntiAsiancall, 
                'Volatility':varsigma}, ignore_index=True)

    dfsigma = dfsigma.set_index('Volatility')  
    f.write(dfsigma.to_latex())        
    print(dfsigma.to_latex())

    print("K tables")
    dfK = pd.DataFrame()
    Ks = [90, 95, 99, 100, 101, 105, 110]
    samples = 100000
    batches = 100
    N = 1000
    for varK in Ks:
        print("Current K: %.2f" % (varK))

        meanPut, CIPut, meanCall, CICall = monteCarloSims(True, False, batches, 
            N, varK, r, S0, T, sigma, eps, seed, False, False, False, asiansteps)
        ATmeanCall, ATCICall = monteCarloSims(False, False, batches, 
            N, varK, r, S0, T, sigma, eps, seed, False, True, False, asiansteps)
        ASmeanCall, ASCICall = monteCarloSims(False, False, batches, 
            N, varK, r, S0, T, sigma, eps, seed, False, False, True, asiansteps)

        AntiEUcall = ATCICall/ATmeanCall
        EUcall = CICall/meanCall
        EUput = CIPut/meanPut
        AntiAsiancall = ASCICall/ASmeanCall

        dfK = dfK.append({'EU Call (AT) Option Price':AntiEUcall, 'EU Call Option Price':EUcall, 
                'EU Put Option Price':EUput, 'Asian Call (AT) Option Price':
                AntiAsiancall, 
                'Strike Price':varK}, ignore_index=True)

    dfK = dfK.set_index('Strike Price')   
    f.write(dfK.to_latex())    
    print(dfK.to_latex())
    f.close()

    print("M table (only for asian)")
    dfM = pd.DataFrame()
    Ms = [365-2*int(365/3), 365-int(365/3), 365, 365-int(365/3), 365+2*int(365/3)]
    Ns = [500, 2000, 3500, 5000, 6500]
    batches = 100
    for varN in Ns:
        print("Current N: %.2f" % (varN))
        resMs = np.array([])
        for varM in Ms:
            print("Current M: %.2f" % (varM))
            ASmeanCall, ASCICall = monteCarloSims(False, False, batches, 
                varN, K, r, S0, T, sigma, eps, seed, False, False, True, varM)
            resMs = np.append(resMs,ASCICall/ASmeanCall)

        dfM = dfM.append({'N':varN, 'M = 121':resMs[0], 'M = 243':resMs[1]
            , 'M = 365':resMs[2], 'M = 486':resMs[3], 'M = 608':resMs[4]}, ignore_index=True)
    
    dfM = dfM.set_index('N')      
    sns.heatmap(dfM, annot=True)
    plt.show()

    """Convergence plots"""
    print("Convergence plots")
    df1 = pd.DataFrame()
    df2 = pd.DataFrame()
    N = 100
    Nmax = 100000
    dN = Nmax/N
    for i in range (1,N+1):
        samples = i*dN
        if(i%10==0): print("%d percent" % i)

        for j in range(0,100):
            AntiEUcall = batchPriceAnti(int(samples/100), K, r, S0, T, sigma, (seed+(j*int(samples/100))))
            df1 = df1.append({'Option Price':AntiEUcall, 'Condition':'European Call (AT)', 'N':samples, 'subject':j}, ignore_index=True)
            AntiAsiancall = batchPriceAsian(int(samples/100), K, r, S0, T, sigma, (seed+int(j*(samples/100))), asiansteps)
            df1 = df1.append({'Option Price':AntiAsiancall, 'Condition':'Asian Call (AT)', 'N':samples, 'subject':j}, ignore_index=True)
            EUcall, EUput = batchPrice(int(samples/100), K, r, S0, T, sigma, (seed+(j*int(samples/100))))
            df2 = df2.append({'Option Price':EUput, 'Condition':'European Put', 'N':samples, 'subject':j}, ignore_index=True)
            df2 = df2.append({'Option Price':EUcall, 'Condition':'European Call', 'N':samples, 'subject':j}, ignore_index=True)
    
    ax1 = sns.tsplot(data=df1, time='N', unit="subject", value = 'Option Price', condition="Condition", ci=[95, 99])
    plt.show()
    ax2 = sns.tsplot(data=df2, time='N', unit="subject", value = 'Option Price', condition="Condition", ci=[95, 99])
    plt.show()

def main(argv):
    """Runs the functions above, can be ran without parameters

    Arguments:
    --samples   sample size per batch
    --seed      seed used
    --batches   number of batches used
    --eps       epsilon used in bump and revalue
    --same      use the same seed for the bump and revalue
    --sens      compute deltas
    --price     compute option prices
    --volat     volatility of the stock
    --strike    strike price
    """

    parser = argparse.ArgumentParser(description='Tell the program what to run with which parameters.')
    parser.add_argument('-n','--samples', type=int, default=100, help='The number of samples in the MC simulation per batch.')
    parser.add_argument('-s','--seed', type=int, default=0, help='The random seed used in the simulation.')
    parser.add_argument('-b','--batches', type=int, default=100, help='The number of batches.')
    parser.add_argument('-e','--eps', type=float, default=0.001, help='The epsilon value used for bump and revalue.')
    parser.add_argument('-k','--strike', type=int, default=99, help='The number of batches.')
    parser.add_argument('-v','--volat', type=float, default=0.2, help='The epsilon value used for bump and revalue.')
    parser.add_argument('--sens', action='store_true', help='Calculate the delta using MC methods.')
    parser.add_argument('--price', action='store_true', help='Calculate the European option prices using MC methods.')
    parser.add_argument('--digit', action='store_true', help='Do the digital option.')
    parser.add_argument('--same', action='store_true', help='Use the same seed for bump and revalue.')
    parser.add_argument('--anti', action='store_true', help='Use antithetic variables and make a comparison.')
    parser.add_argument('--asian', action='store_true', help='Evaluate an Asian call option.')
    parser.add_argument('--asiansteps', type=float, default=365, help='The no. of steps used for evaluating an Asian call, standard is daily.')
    parser.add_argument('--plots', action='store_true', help='Run all plots (Seaborne with high accuracy, takes long).')
    args = parser.parse_args()
    
    N = args.samples
    seed = args.seed
    batches = args.batches
    eps = args.eps
    sigma = args.volat
    K = args.strike
    r = 0.06
    S0 = 100
    T = 1

    if(args.plots):
        plots(K, r, S0, T, sigma, eps, seed, args.asiansteps)

    if(args.price):
        meanPut, CIPut, meanCall, CICall = monteCarloSims(args.price, args.sens, batches, 
            N, K, r, S0, T, sigma, eps, seed, args.same, args.anti, args.asian, args.asiansteps)
        print("We find a mean European call option price of %.3f with a 95 percent CI ranging from %.2f to %.2f, using %d samples. CI width is %.3f." 
            % (meanCall, (meanCall-CICall), (meanCall+CICall), (batches*N), CICall))
        print("We find a mean European put option price of %.3f with a 95 percent CI ranging from %.2f to %.2f, using %d samples. CI width is %.3f." 
            % (meanPut, (meanPut-CIPut), (meanPut+CIPut), (batches*N), CIPut))

    if(args.sens):
        meanPut, CIPut, meanCall, CICall = monteCarloSims(args.price, args.sens, batches, 
            N, K, r, S0, T, sigma, eps, seed, args.same, args.anti, args.asian, args.asiansteps)

        print("We find a mean call delta of %.6f with a 95 percent CI ranging from %.6f to %.6f, using %d samples." 
            % (meanCall, (meanCall-CICall), (meanCall+CICall), (batches*N)))
        print("We find a mean put delta of %.6f with a 95 percent CI ranging from %.6f to %.6f, using %d samples." 
            % (meanPut, (meanPut-CIPut), (meanPut+CIPut), (batches*N)))  

    if(args.digit):
        #meanPut_d, CIPut_d, meanCall_d, CICall_d, meanPutLR, CIPutLR, meanCallLR, CICallLR = monteCarloSimsDigital(args.price, args.sens, batches, N, K, r, S0, T, sigma, eps, seed, args.same)
        #meanPut_d, CIPut_d, meanCall_d, CICall_d = monteCarloSimsDigital(args.price, args.sens, batches, N, K, r, S0, T, sigma, eps, seed, args.same)

        (pricePutTh, meanPut_d, CIPut_d, priceCallTh, meanCall_d, CICall_d, deltaMeanPutLR, deltaCIPutLR, deltaMeanCallLR, deltaCICallLR, deltaCallTh, deltaPutTh) =\
         monteCarloSimsDigital(batches, N, K, r, S0, T, sigma, eps, seed, args.same)

        print("We find a mean Digital European call option price of %.2f with a 95 percent CI ranging from %.2f to %.2f, using %d samples." 
            % (meanCall_d, (meanCall_d-CICall_d), (meanCall_d+CICall_d), (batches*N)))
        print("We find a mean Digital European put option price of %.2f with a 95 percent CI ranging from %.2f to %.2f, using %d samples." 
            % (meanPut_d, (meanPut_d-CIPut_d), (meanPut_d+CIPut_d), (batches*N)))           

        print("Using Likelihood Ratio method, \nWe find a mean call (digital) delta of %.6f with a 95 percent CI ranging from %.6f to %.6f, using %d samples." 
            % (deltaMeanCallLR, (deltaMeanCallLR-deltaCICallLR), (deltaMeanCallLR+deltaCICallLR), (batches*N)))
        print("We find a mean put (digital) delta of %.6f with a 95 percent CI ranging from %.6f to %.6f, using %d samples." 
            % (deltaMeanPutLR, (deltaMeanPutLR-deltaCIPutLR), (deltaMeanPutLR+deltaCIPutLR), (batches*N)))

        #plotDeltaLikelihoodRatio(batches, N, K, r, S0, T, sigma, eps, seed, args.same)
        

        meanPut_dArray = []
        CIPut_dArray = []
        meanCall_dArray = []
        CICall_dArray = []
        
        deltaMeanPutLRArray = []
        deltaCIPutLRArray = []
        deltaMeanCallLRArray = []
        deltaCICallLRArray = []
        N_b = N
        for Ni in range(10,N_b,10):
            (pricePutTh, meanPut_d, CIPut_d, priceCallTh, meanCall_d, CICall_d, deltaMeanPutLR, deltaCIPutLR, deltaMeanCallLR, deltaCICallLR, deltaCallTh, deltaPutTh) =\
            monteCarloSimsDigital(batches, Ni, K, r, S0, T, sigma, eps, seed, args.same)

            meanPut_dArray.append(meanPut_d)
            CIPut_dArray.append(CIPut_d)
            meanCall_dArray.append(meanCall_d)
            CICall_dArray.append(CICall_d)

            deltaMeanPutLRArray.append(deltaMeanPutLR)
            deltaCIPutLRArray.append(deltaCIPutLR)
            deltaMeanCallLRArray.append(deltaMeanCallLR)
            deltaCICallLRArray.append(deltaCICallLR)

        pricePutUpperCI = [x + y for x, y in zip(meanPut_dArray, CIPut_dArray)]
        pricePutLowerCI = [x - y for x, y in zip(meanPut_dArray, CIPut_dArray)]
        priceCallUpperCI = [x + y for x, y in zip(meanCall_dArray, CICall_dArray)]
        priceCallLowerCI = [x - y for x, y in zip(meanCall_dArray, CICall_dArray)]

        deltaPutLR_upCI = [x + y for x, y in zip(deltaMeanPutLRArray, deltaCIPutLRArray)]
        deltaPutLR_lowCI = [x - y for x, y in zip(deltaMeanPutLRArray, deltaCIPutLRArray)]
        deltaCallLR_upCI = [x + y for x, y in zip(deltaMeanCallLRArray, deltaCICallLRArray)]
        deltaCallLR_lowCI = [x - y for x, y in zip(deltaMeanCallLRArray, deltaCICallLRArray)]

        # Data for Plot of delta as a function of sigma
        # sigmaTrials = [0.01, 0.05, 0.1, 0.2]
        # maxPrice = 200
        # deltaPutThArray = np.zeros((len(sigmaTrials),maxPrice))
        # for sindex, sigma_i in enumerate(sigmaTrials):
        #     for StockVal in range(1, maxPrice):
        #         d2_new = (np.log(StockVal/float(K)) + (r-0.5*sigmaTrials[sindex]*sigmaTrials[sindex])*T) / sigmaTrials[sindex]*math.sqrt(T)
        #         Nd2_new = ( 1/ math.sqrt(2*math.pi) ) * math.exp(-0.5*d2_new*d2_new)
        #         deltaPutThArray[sindex][StockVal-1]  = -1* ( math.exp(-r*T) * Nd2_new) / (sigmaTrials[sindex]*StockVal*math.sqrt(T))
        
        # deltaCallThArray = np.zeros((len(sigmaTrials),maxPrice))
        # for sindex, sigma_i in enumerate(sigmaTrials):
        #     for StockVal in range(1, maxPrice):
        #         d2_new = (np.log(StockVal/float(K)) + (r-0.5*sigmaTrials[sindex]*sigmaTrials[sindex])*T) / sigmaTrials[sindex]*math.sqrt(T)
        #         Nd2_new = ( 1/ math.sqrt(2*math.pi) ) * math.exp(-0.5*d2_new*d2_new)
        #         deltaCallThArray[sindex][StockVal-1]  = ( math.exp(-r*T) * Nd2_new) / (sigmaTrials[sindex]*StockVal*math.sqrt(T))


        

        # priceCallUpperCI = [x + y for x, y in zip(meanCall_dArray, CICall_dArray)]
        # priceCallLowerCI = [x - y for x, y in zip(meanCall_dArray, CICall_dArray)]
        # plt.plot(meanCall_dArray[:])
        # plt.plot(priceCallUpperCI)
        # plt.plot(priceCallLowerCI)
        # plt.axhline(y=priceCallTh)
        # plt.show()

        # deltaPutUpperCI = [x + y for x, y in zip(deltaMeanPutLRArray, deltaCIPutLRArray)]
        # deltaPutLowerCI = [x - y for x, y in zip(deltaMeanPutLRArray, deltaCIPutLRArray)]
        # plt.plot(deltaMeanPutLRArray[:])
        # plt.plot(deltaPutUpperCI)
        # plt.plot(deltaPutLowerCI)
        # plt.axhline(y=deltaPutTh)
        # plt.show()
        # deltaCallUpperCI = [x + y for x, y in zip(deltaMeanCallLRArray, deltaCICallLRArray)]
        # deltaCallLowerCI = [x - y for x, y in zip(deltaMeanCallLRArray, deltaCICallLRArray)]
        # plt.plot(deltaMeanCallLRArray[:])
        # plt.plot(deltaCallUpperCI)
        # plt.plot(deltaCallLowerCI)
        # plt.axhline(y=deltaCallTh)
        # plt.show()


        my_dpi = 200
        Xfig = 2000
        Yfig = 2000
        plt.figure(figsize=(Xfig/my_dpi, Yfig/my_dpi), dpi=my_dpi)

        # DELTA PUT PLOT as a function of sigma
        # plt.plot(deltaPutThArray[0][:],'dodgerblue',label=r'$\sigma$ = 0.01')
        # plt.plot(deltaPutThArray[1][:],'skyblue',label=r'$\sigma$ = 0.05')
        # plt.plot(deltaPutThArray[2][:],'cadetblue',label=r'$\sigma$ = 0.1')
        # plt.plot(deltaPutThArray[3][:],'darkslategrey',label=r'$\sigma$ = 0.2')
        # handles, labels = plt.gca().get_legend_handles_labels()
        # by_label = OrderedDict(zip(labels, handles))
        # plt.legend(by_label.values(), by_label.keys(),fontsize=16,loc=4)
        # plt.xlabel('Stock Price', fontsize=16)
        # plt.ylabel('Put Option Delta', fontsize=16)
        # plt.savefig('deltaPut1.png',dpi=my_dpi)
        # plt.clf() 
        # plt.cla() 
        # DELTA CALL PLOT as a function of sigma
        # plt.plot(deltaCallThArray[0][:],'dodgerblue',label=r'$\sigma$ = 0.01')
        # plt.plot(deltaCallThArray[1][:],'skyblue',label=r'$\sigma$ = 0.05')
        # plt.plot(deltaCallThArray[2][:],'cadetblue',label=r'$\sigma$ = 0.1')
        # plt.plot(deltaCallThArray[3][:],'darkslategrey',label=r'$\sigma$ = 0.2')
        # handles, labels = plt.gca().get_legend_handles_labels()
        # by_label = OrderedDict(zip(labels, handles))
        # plt.legend(by_label.values(), by_label.keys(),fontsize=16,loc=4)
        # plt.xlabel('Stock Price', fontsize=16)
        # plt.ylabel('Call Option Delta', fontsize=16)
        # plt.savefig('deltaCall1.png',dpi=my_dpi)
        # plt.clf() 
        # plt.cla() 

        plt.plot(meanPut_dArray, 'b', label='Simulated Price Mean')
        plt.axhline(y=pricePutTh, color='green', ls='--',label='Theoretical Price')
        plt.plot(pricePutUpperCI, 'gray', label='95% Confidence Interval')
        plt.plot(pricePutLowerCI, 'gray')


        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys(),fontsize=16)

        plt.xlabel('N', fontsize=16)
        plt.ylabel('Option Value', fontsize=16)
        #plt.grid(True)
        #plt.title(r'$\rho=%s, \  n=%s, \ \mu=%s,\ \lambda=%s $ '%(rho, n, mu, lamb) +' \n $Batch \ Size=%s, \ Excluded \ First \ %s \ Batches,  \ Total \ Customers=%s $' %(batch_size, excluding_batches, customers), fontsize=16)
        #plt.text(0.5*customers, 0.2*Wq, '$M =%.4g$\n$Z =%.4g$\n$S^2=%.4g$'%(BatchMeansMean,plusminus,BatchMeansMean_VAR), fontsize=16, bbox={'facecolor':'white', 'alpha':0.8, 'pad':5})
        #plt.text(customers-batch_size, 1.1*Wq, r'$Estimate=%.2f$'%(BatchMeansMean), fontsize=16)
        #plt.axis([0, customers, 0, Wq*3])
        #plt.savefig('IMG_'+'_PRIOR_SERVERS_'+str(n)+'__RHO_'+str(rho)+'__NUMBER_'+str(imgN)+'.png',dpi=my_dpi)
        plt.savefig('put1.png',dpi=my_dpi)
        
        plt.clf() 
        plt.cla()


        plt.plot(meanCall_dArray, 'b', label='Simulated Price Mean')
        plt.axhline(y=priceCallTh, color='green', ls='--',label='Theoretical Price')
        plt.plot(priceCallUpperCI, 'gray', label='95% Confidence Interval')
        plt.plot(priceCallLowerCI, 'gray')
        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys(),fontsize=16)
        plt.xlabel('N', fontsize=16)
        plt.ylabel('Option Value', fontsize=16)
        plt.savefig('call1.png',dpi=my_dpi)
        plt.clf() 
        plt.cla() 

        plt.plot(deltaMeanPutLRArray, 'r', label='Simulated Delta')
        plt.axhline(y=deltaPutTh, color='b', ls='--',label='Theoretical Delta')
        plt.plot(deltaPutLR_upCI, 'gray', label='95% Confidence Interval')
        plt.plot(deltaPutLR_lowCI, 'gray')
        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys(),fontsize=16)
        plt.xlabel('N', fontsize=16)
        plt.ylabel('Delta', fontsize=16)
        plt.savefig('put_LR_Delta.png',dpi=my_dpi)
        plt.clf() 
        plt.cla() 
        plt.plot(deltaMeanCallLRArray, 'r', label='Simulated Delta')
        plt.axhline(y=deltaCallTh, color='b', ls='--',label='Theoretical Delta')
        plt.plot(deltaCallLR_upCI, 'gray', label='95% Confidence Interval')
        plt.plot(deltaCallLR_lowCI, 'gray')
        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys(),fontsize=16)
        plt.xlabel('N', fontsize=16)
        plt.ylabel('Delta', fontsize=16)
        plt.savefig('call_LR_Delta.png',dpi=my_dpi)
        plt.clf() 
        plt.cla() 

    if(args.anti):
        # For reference:
        meanPut, CIPut, meanCall, CICall = monteCarloSims(True, False, batches, 
            N, K, r, S0, T, sigma, eps, seed, args.same, False, args.asian, args.asiansteps)
        print("We find a mean European call option price of %.3f with a 95 percent CI ranging from %.3f to %.3f, using %d samples. CI width is %.3f." 
            % (meanCall, (meanCall-CICall), (meanCall+CICall), (batches*N), CICall))

        meanCall, CICall = monteCarloSims(args.price, args.sens, batches, 
            N, K, r, S0, T, sigma, eps, seed, args.same, args.anti, args.asian, args.asiansteps)
        print("With antithetic variables we find a mean European call option price of %.3f with a 95 percent CI ranging from %.3f to %.3f, using %d samples.CI width is %.3f." 
            % (meanCall, (meanCall-CICall), (meanCall+CICall), (batches*N), CICall))

    if(args.asian):
        meanCall, CICall = monteCarloSims(args.price, args.sens, batches, 
            N, K, r, S0, T, sigma, eps, seed, args.same, args.anti, args.asian, args.asiansteps)
        print("With antithetic variables we find a mean Asian call option price of %.3f with a 95 percent CI ranging from %.3f to %.3f, using %d samples and %d timesteps. CI width is %.3f." 
            % (meanCall, (meanCall-CICall), (meanCall+CICall), (batches*N), args.asiansteps, CICall))


if __name__ == "__main__":
    main(sys.argv)