import math
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import argparse
import random
from scipy.stats import norm

"""Comment box, clear once you read them :)

-   Finished the bonus, I think!
-   Added some new commands: --seed sets the seed,
    and -- ratio sets the ratio between the two
    volatilities. The option volatility is still set
    using --vol
"""

# Variables currently held constant
K = 99        # strike price of the option
r = 0.06      # one year interest rate
s_0 = 100     # initial price of the stock
T = 1         # one year

def formTreeEUCall(steps,volatility):
    """Receives a number of steps and the volatility, output a 2D list of stock prices and option values 

    This function determines the option value at each timestep for a European call
    Key arrays:
    stock     -- 2D array containing all possible stock values for each timestep
    valvect   -- vector of the option values at each timestep (i.e. step of maturity)
    option    -- 2D array containing all possible option values
    """

    # Variables depending on the number of timesteps
    u =  math.exp(volatility * math.sqrt(T/float(steps)))
    d = 1/u
    a = math.exp(r*T/float(steps))
    p = (a-d)/(u-d)

    # (Step 1) 2D array containing all stock values 
    stock = [[0 for x in range(steps)] for x in range(steps)] 
    stock[0][0] = s_0
    for i in range(1, steps):
        for j in range(0, (i+1)):
            if j == 0: 
                stock[j][i] = stock[j][i-1]*u        
            else: 
                stock[j][i] = stock[j-1][i-1]*d

    # (Step 2) Compute the option values at the terminal nodes: take vector of stock prices and apply payoff function
    valVect=[]
    for j in range(0, steps):
        valVect.append(max(0,stock[j][steps-1]-K))

    # (Step 3) Backtrack option value all the way to state zero
    option = [[0 for x in range(steps)] for x in range(steps)]
    for j in range(0, steps):
        option[j][steps-1] = valVect[j]
    for i in range(steps-2, -1, -1):
        for j in range(0, (i+1)):
            option[j][i] = ( option[j][i+1] * p + option[j+1][i+1] * (1-p) ) * math.exp(-r*T/float(steps))

    return(stock,option)

def formTreeAmericanCall(steps,volatility):
    """Receives a number of steps and the volatility, output a 2D list of stock prices and option values 

    This function determines the option value at each timestep for a European call
    Key arrays:
    stock     -- 2D array containing all possible stock values for each timestep
    valvect   -- vector of the option values at each timestep (i.e. step of maturity)
    option    -- 2D array containing all possible option values
    """

    # Variables depending on the number of timesteps
    u =  math.exp(volatility * math.sqrt(T/float(steps)))
    d = 1/u
    a = math.exp(r*T/float(steps))
    p = (a-d)/(u-d)
    
    # (Step 1) 2D array containing all stock values 
    stock = [[0 for x in range(steps)] for x in range(steps)] 
    stock[0][0] = s_0
    for i in range(1, steps):
        for j in range(0, (i+1)):
            if j == 0: 
                stock[j][i] = stock[j][i-1]*u        
            else: 
                stock[j][i] = stock[j-1][i-1]*d

    # (Step 2) Compute the option values at the terminal nodes: take vector of stock prices and apply payoff function
    valVect=[]
    for j in range(0, steps):
        valVect.append(max(0,stock[j][steps-1]-K))

    # (Step 3) Backtrack option value all the way to state zero
    option = [[0 for x in range(steps)] for x in range(steps)]
    for j in range(0, steps):
        option[j][steps-1] = valVect[j]
    for i in range(steps-2, -1, -1):
        for j in range(0, (i+1)):
            op = ( option[j][i+1] * p + option[j+1][i+1] * (1-p) ) * math.exp(-r*T/float(steps))
            exercise = stock[j][i] - K
            option[j][i] = max(op, exercise)

    return(stock,option)

def formTreeEUPut(steps,volatility):
    """Receives a number of steps and the volatility, output a 2D list of stock prices and option values.
    Now we examine a scenario where we buy a right to sell!

    This function determines the option value at each timestep for a European call
    Key arrays:
    stock     -- 2D array containing all possible stock values for each timestep
    valvect   -- vector of the option values at each timestep (i.e. step of maturity)
    option    -- 2D array containing all possible option values
    """

    # Variables depending on the number of timesteps
    u =  math.exp(volatility * math.sqrt(T/float(steps)))
    d = 1/u
    a = math.exp(r*T/float(steps))
    p = (a-d)/(u-d)
    
    # (Step 1) 2D array containing all stock values 
    stock = [[0 for x in range(steps)] for x in range(steps)] 
    stock[0][0] = s_0
    for i in range(1, steps):
        for j in range(0, (i+1)):
            if j == 0: 
                stock[j][i] = stock[j][i-1]*u        
            else: 
                stock[j][i] = stock[j-1][i-1]*d

    # (Step 2) Compute the option values at the terminal nodes: take vector of stock prices and apply payoff function
    valVect=[]
    for j in range(0, steps):
        valVect.append(max(0,K-stock[j][steps-1]))

    # (Step 3) Backtrack option value all the way to state zero
    option = [[0 for x in range(steps)] for x in range(steps)]
    for j in range(0, steps):
        option[j][steps-1] = valVect[j]
    for i in range(steps-2, -1, -1):
        for j in range(0, (i+1)):
            option[j][i] = ( option[j][i+1] * p + option[j+1][i+1] * (1-p) ) * math.exp(-r*T/float(steps))

    return(stock,option)

def formTreeAmericanPut(steps,volatility):
    """Receives a number of steps and the volatility, output a 2D list of stock prices and option values.
    Now we examine a scenario where we buy a right to sell!

    This function determines the option value at each timestep for a European call
    Key arrays:
    stock     -- 2D array containing all possible stock values for each timestep
    valvect   -- vector of the option values at each timestep (i.e. step of maturity)
    option    -- 2D array containing all possible option values
    """

    # Variables depending on the number of timesteps
    u =  math.exp(volatility * math.sqrt(T/float(steps)))
    d = 1/u
    a = math.exp(r*T/float(steps))
    p = (a-d)/(u-d)

    # (Step 1) 2D array containing all stock values 
    stock = [[0 for x in range(steps)] for x in range(steps)] 
    stock[0][0] = s_0
    for i in range(1, steps):
        for j in range(0, (i+1)):
            if j == 0: 
                stock[j][i] = stock[j][i-1]*u        
            else: 
                stock[j][i] = stock[j-1][i-1]*d

    # (Step 2) Compute the option values at the terminal nodes: take vector of stock prices and apply payoff function
    valVect=[]
    for j in range(0, steps):
        valVect.append(max(0,K-stock[j][steps-1]))

    # (Step 3) Backtrack option value all the way to state zero
    option = [[0 for x in range(steps)] for x in range(steps)]
    for j in range(0, steps):
        option[j][steps-1] = valVect[j]
    for i in range(steps-2, -1, -1):
        for j in range(0, (i+1)):
            op = ( option[j][i+1] * p + option[j+1][i+1] * (1-p) ) * math.exp(-r*T/float(steps))
            exercise = K - stock[j][i]
            option[j][i] = max(op, exercise)
    return(stock,option)


def computeDelta(stock,option):
    """Returns delta, the hedging parameter"""
    delta =  (option[0][1] - option[1][1]) / (stock[0][1] - stock[1][1])
    return delta
    
def callPutParity(c,p):
    """Returns right and left parts of call put parity equation.

    Input: call premium, put premium
    """
    K = 99         # strike price of the option
    r = 0.06     # one year interest rate
    s_0 = 100     # initial price of the stock
    T = 1
    parityLeft = c+K*math.exp(-r/float(T))
    parityRight = p+s_0
    return parityLeft, parityRight

def maxItemLength(a):
    """Returns the maximum length of all the items in the array"""
 
    maxLen = 0
    rows = len(a)
    cols = len(a[0])
    for row in range(rows):
        for col in range(cols):
            maxLen = max(maxLen, len(str(a[row][col])))
    return maxLen

def print2dList(a):
    """Input is a 2D list, prints the list"""

    if (a == []):
        print("Error: this array is empty!")
        return
    rows = len(a)
    cols = len(a[0])
    fieldWidth = maxItemLength(a)
    print("[ ",end=" ")
    for row in range(rows):
        if (row > 0): print("\n  ",end=" ")
        print("[ ",end=" ")
        for col in range(cols):
            if (col > 0): print(",",end=" ")
            # The next 2 lines print a[row][col] with the given fieldWidth
            format = "%" + str(fieldWidth) + "s"
            print(format % str(a[row][col]),end=" ")
        print("]",end=" ")
    print("]")

def testComplexity(dataPoints, volatility):
    """Tests if the algorithm as an O(n^2) timecomplexity

    Returns two vectors of datapoints:
    xValues    -- number of timesteps
    yValues    -- number of seconds
    """

    xValues = [100*x for x in range(1,dataPoints)]
    yValues = []
    for xVal in xValues:
        start_time = time.time()
        formTreeEUCall(xVal, volatility)
        total_time = time.time() - start_time
        yValues.append(total_time)
    return(xValues,yValues)

def ratePerTimesteps(dataPoints, volatility):
    """Provides the data for a plot of the no of timesteps versus the EUCall 
    value at t=0

    Returns two vectors of datapoints:
    xValues    -- number of timesteps
    yValues    -- number of seconds
    """

    xValues = [5*x for x in range(50,dataPoints)]
    yValues = []
    y2Values = []
    for xVal in xValues:
        (stock,option) = formTreeEUCall(xVal, volatility)
        yValues.append(11.544280-option[0][0])
        (stock,option) = formTreeEUPut(xVal, volatility)
        y2Values.append(4.77896905-option[0][0])
    return(xValues,yValues,y2Values)

def bonusAssign(timesteps, optVol, stockVol, seed):
    """Simulates a stock and the option prices for several timesteps
    Uses a Wiener process to simulate the stock
    Timespan is a year in total, timesteps divide up the year

    Returns vectors of datapoints of stockprices and deltas
    """
    xs = []
    deltas = []
    stockprices = []
    if (timesteps < 367):
        dt = 1/timesteps
        dailystockprices = [100]
        
        # reverse norm.ppf(norm.cdf(1.96))
        np.random.seed(seed=seed)
        for i in range(1,365):
            S = dailystockprices[i-1]
            mu = dailystockprices[i-1] * np.exp(r * dt) - dailystockprices[i-1]
            dS = mu + stockVol * S * math.sqrt(dt) * np.random.normal(0,1)
            dailystockprices.append(S + dS)

        d1s = []
        for i in range(0,timesteps):
            if((1-dt*i) ==0): i = timesteps - 0.001
            xs.append(i*365*dt)
            stockprices.append(dailystockprices[int(i*365*dt)])
            d1s.append((1/(optVol * np.sqrt(1-dt*i))) * (np.log(stockprices[i]/K)+(r+(optVol*optVol/2))*(1-dt*i)))
            deltas.append(norm.cdf(d1s[i]))
        xs[timesteps-1] = 365
    return (xs, stockprices, deltas)

def main(argv):
    """Runs the functions above, can be ran without parameters

    Arguments:
    --steps      the number of timesteps, skips if 0, standard is 10 (1 integer argument)
    --vol        set the volatility, standard is 0.2 (1 integer argument)
    --datapoints the number of datapoints in the complexity plot, does not plot if 0, standard is 0 (1 integer argument)
    --runPlot    plot the option prices
    --bonus      run bonus assignment
    """

    parser = argparse.ArgumentParser(description='Tell the program what to run with which parameters.')
    parser.add_argument('-s','--steps', type=int, default=10, help='The number of timesteps.')
    parser.add_argument('-v','--vol', type=float, default=0.2, help='Volatility in the simulation.')
    parser.add_argument('-c','--datapoints', type=int, default=0, 
        help='The number of timestepsin the timecomplexity simulation.')
    parser.add_argument('-p','--runPlot', action='store_true', help='Make a convergence plot.')
    parser.add_argument('-b','--bonus', action='store_true', help='Run the bonus.')
    parser.add_argument('--seed', type=int, default=1, help='Set the seed of the hedging simulation (bonus only).')
    parser.add_argument('--ratio', type=float, default=1, help='Ratio of the option volatility over the stock volatility (bonus only).')
    args = parser.parse_args()
    complexityPoints = args.datapoints
    steps = args.steps
    seed = args.seed
    volatility = float(args.vol)
    ratio = args.ratio

    if(not(steps<1)):
        """First let's do the European Call"""
        stock, option = formTreeEUCall(steps,volatility)
        deltaCall = computeDelta(stock,option)
        c = option[0][0] #for input of callPutParity
        if(steps < 9):
            print("Stock prices:")
            print2dList(stock)
            print("Call option values:")
            print2dList(option)
        print("Call premium should be %.2f dollars" % option[0][0])

        """Second let's do the European Put"""
        stock, option = formTreeEUPut(steps,volatility)
        deltaPut = computeDelta(stock,option)
        p = option[0][0] #for input of callPutParity
        if(steps < 9):
            print("Put option values:")
            print2dList(option)
        print("Put premium should be %.2f dollars" % option[0][0])
        
        """We can now see if we satisfy the put-call parity and output the hedge parameters"""
        print("Delta (EU Call) =",deltaCall, "\nDelta (EU Put) =",deltaPut)
        parityLeft, parityRight = callPutParity(c,p)
        print("Call Put parity: %.7f ? %.7f " % (parityLeft, parityRight) )

        """We can also do the American Call!"""
        stock, option = formTreeAmericanCall(steps,volatility)
        deltaCall = computeDelta(stock,option)
        c = option[0][0] #for input of callPutParity
        if(steps < 9):
            print("American call option values:")
            print2dList(option)
        print("Call premium should be %.2f dollars" % option[0][0])

        """We can also do the American Put!"""
        stock, option = formTreeAmericanPut(steps,volatility)
        deltaPut = computeDelta(stock,option)
        p = option[0][0] #for input of callPutParity
        if(steps < 9):
            print("American put option values:")
            print2dList(option)
        print("Put premium should be %.2f dollars" % option[0][0])

        """We can now see if we satisfy the put-call parity and output the hedge parameters"""
        print("Delta (American Call) =",deltaCall, "\nDelta (American Put) =",deltaPut)
        parityLeft, parityRight = callPutParity(c,p)
        # print("Call Put parity: %.7f ? %.7f " % (parityLeft, parityRight) )

    if(not(complexityPoints<1)):
        print("Now plot the time complexity...")
        x,y = testComplexity(complexityPoints, volatility)
        plt.xlabel("No. of Timesteps")
        plt.ylabel("Time (seconds)")
        plt.plot(x,y)
        plt.show()

    if args.runPlot:
        x,y,y2 = ratePerTimesteps(200, volatility)
        plt.xlabel("No. of Timesteps")
        plt.ylabel("Error in option price (t=0)")
        plt.plot(x,y,label="Call option")
        plt.plot(x,y2,label="Put option")
        plt.legend(bbox_to_anchor=(0.25, 0.97),fontsize=11)
        plt.show()

    if args.bonus:
        optionVol = volatility
        stockVol = optionVol/ratio
        print("Using option volatility %.2f and stock volatility %.2f."
            % (optionVol, stockVol))
        xsDay, stocksDay, deltasDay = bonusAssign(365,optionVol,stockVol,seed)
        xsWeek, stocksWeek, deltasWeek = bonusAssign(52,optionVol,stockVol,seed)
        plt.plot(xsDay,stocksDay)
        plt.title("Daily price of stock")
        plt.xlabel("Day")
        plt.ylabel("Hedge parameter")
        plt.xlim(0,365)
        plt.show()
        plt.xlabel("Day")
        plt.ylabel("Hedge parameter")
        plt.plot(xsDay,deltasDay,label="Daily updates")
        plt.plot(xsWeek,deltasWeek,label="Weekly updates")
        plt.legend(bbox_to_anchor=(0.35, 0.97),fontsize=11)
        plt.xlim(0,365)
        plt.ylim(0,1.1)
        plt.show()

        # now to evaluate the RMSE
        xsDay, stocksDay, deltasDayRef = bonusAssign(365,optionVol,stockVol,seed)
        vals = [1/16, 1/8, 1/4, 1/2, 2, 4, 8, 16]
        for val in vals:
            optVolVal = 0.2 * val
            print(optVolVal)
            xsDay, stocksDay, deltasDay = bonusAssign(365,optVolVal,0.2,seed)
            error = np.subtract(deltasDay,deltasDayRef)
            SE = error*error
            MSE = sum(SE)/len(SE)
            RMSE = np.sqrt(MSE)
            print("RMSE for this option volatility (stock is 0.2) is %.3f" %RMSE)

        for val in vals:
            optVolVal = 0.2 * val
            print(optVolVal)
            xsDay, stocksDay, deltasDay = bonusAssign(365,0.2,optVolVal,seed)
            error = np.subtract(deltasDay,deltasDayRef)
            SE = error*error
            MSE = sum(SE)/len(SE)
            RMSE = np.sqrt(MSE)
            print("RMSE for this stock volatility (option is 0.2) is %.3f" %RMSE)

if __name__ == "__main__":
    main(sys.argv)